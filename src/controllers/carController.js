const boom = require('boom'),
    Car = require('../models/Car');

    // get all cars
    exports.getCars = async (req,reply)=>{
        try {
            const cars = await Car.find()
            return cars
        } catch (error) {
            throw boom.boomify(err)
        }
    }

    // get single car by id
    exports.getSingleCar = async (req,reply)=>{
        try {
            const id = req.params.id
            const car = await Car.findById(id)
            return car
        } catch (error) {
            throw boom.boomify(error)
        }
    }

    // add new car
    exports.addCar = async (req,reply)=>{
        try {
            const car = new Car(req.body)
            return car.save()
        } catch (error) {
            throw boom.boomify(error)
        }
    }

    // update an existing car
    exports.updateCar = async (req,reply)=>{
        try {
            const id = req.params.id
            const car = req.body
            const {...upddateData } = car
            const update = await Car.findByIdAndUpdate(id, upddateData, {new : true})
            return update
        } catch (error) {
            throw boom.boomify(error)
        }
    }

    // delete a car
    exports.deleteCar = async (req,reply)=>{
        try {
            const id = req.params.id
            const car = await Car.findByIdAndRemove(id)
            return car
        } catch (error) {
            throw boom.boomify(error)
        }
    }