const fastify = require('fastify')({
    logger: true
})
const mongoose = require('mongoose');
const routes = require('../routes/route')
const swagger = require('../config/swagger')

fastify.register(require('fastify-swagger'), swagger.options)

//database
mongoose.connect(`mongodb://localhost:27017`)
    .then(()=> console.log('MongoDB Connected'))
    .catch(err=> console.log(err))

//route
fastify.get('/', async (request, reply)=>{
    return { hello : 'World'}
})

routes.forEach((route, index)=>{
    fastify.route(route)
})

const start = async ()=>{
    try {
        await fastify.listen(3000)
        fastify.swagger()
        fastify.log.info(`server listening on ${fastify.server.address().port}`)
    } catch (error) {
        fastify.log.error(error)
        process.exit(1)
    }
}
start()